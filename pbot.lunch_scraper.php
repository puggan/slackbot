<?php

	require_once(__DIR__ . "/remote.php");
	require_once(__DIR__ . "/pbot.config.php");

	$url = "http://www.butlercatering.se/einstein";
	$food_page = $remote_site->get_page($url);
	if($food_page AND $remote_site->info['http_code'] == 200)
	{
		file_put_contents("foodpage.html", $food_page);
	}
	else
	{
		$food_page = file_get_contents("foodpage.html");
	}

	$url = "https://slack.com/api/chat.postMessage";

	$data = array("token" => $key);
	$data['channel'] = 'C0B8QR8TC';
	$data['username'] = 'P-Bot';
	$data['icon_url'] = 'https://avatars.slack-edge.com/2015-09-25/11365147074_d9fad6a6d32b9fb561ee_192.jpg';

	$current_week = date("W");
	$food_week = (int) substr(strstr(strstr($food_page, '<h2 class="lunch-titel">'), '</h2>', TRUE), -2);
	if($food_week != $current_week)
	{
		$data['text'] = "Menyn för vecka {$current_week} saknas, hittade menyn för vecka {$food_week} :-(";
	}
	else
	{
		$food_days = explode('<div class="field-day">', strstr($food_page, '<p>Veckans', TRUE));
		$food_day = $food_days[date("w")];
		$food = explode("\n", trim(strip_tags($food_day)));
		print_r($food);

		$data['text'] = ":meat_on_bone: Kött: " . trim($food[2]) . PHP_EOL . ":fish: Fisk: " . trim($food[1]);

		if(date("w") == 5)
		{
			$data['text'] .= PHP_EOL . ":icecream: Is it Friday? Yes! ICE CREAM!!!";
		}
	}

	$test = $remote_site->post_page($url, $data);

	var_dump($test);
