<?php

	require_once(__DIR__ . "/remote.php");
	require_once(__DIR__ . "/pbot.config.php");

	$url = "https://slack.com/api/channels.list";

	$data = array("token" => $key);
	$data['exclude_archived'] = 1;
	$result = $remote_site->post_page($url, $data);

	foreach(json_decode($result)->channels as $row)
	{
		echo "{$row->id}\t{$row->name}\n";
	}
